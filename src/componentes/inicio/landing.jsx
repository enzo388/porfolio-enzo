import React, { useEffect } from 'react';

import "../inicio/landing.css"
import VideoLanding2 from "../../media/videoFondo1.mp4"
import enzo from "../../media/enzoBETA1.png";
import food from "../../media/foodCapture.PNG";
import pf from "../../media/Pf.PNG";
import flight from "../../media/Flyght.PNG";
import pokemon from "../../media/pokemon.png";
import aluxion from "../../media/aluxion.PNG";


import selfControl from "../../media/selfControl.mp3"
import Success from "../../media/Success.mp3"
import mix from "../../media/mix.mp3"
import Push from "../../media/Push.mp3"
import Shake_It_Up from "../../media/Shake_It_Up.mp3"
import Take from "../../media/Take.mp3"
import Shes_On_Fire from "../../media/Shes_On_Fire.mp3"
import Right_Combination from "../../media/Right_Combination.mp3"
import ReactAudioPlayer from 'react-audio-player';

import Nav from ".//boton"

import Link from "@mui/material/Link";
import { gsap } from "gsap";



function Landing() {

    const [musica, setMusica] = React.useState(selfControl);
    const [numeroDeTrack, setNumeroDeTrack] = React.useState(0);
    const [animacion, setAnimacion] = React.useState(0);
    const playList = [
        Push, Right_Combination, Success, selfControl, mix, Shake_It_Up, Take, Shes_On_Fire
    ]

    const scrollToTop = () => {
        document.getElementById("videoFondo").scroll(0,0)
      }
      
    useEffect(() => {
        if (animacion === 0) {
            
            const divInfoAzul = document.querySelector(".divInfoAzul")
            gsap.from(divInfoAzul, { y:1900, delay:10 })
            setAnimacion(1)
            scrollToTop()
        }

        /* cambiamelamusica() */
    },[animacion])

    function cambiamelamusica() {
        setInterval(() => {
            handlerPlayList()
        }, 180000);
        return musica
    }



    function handlerPlayList() {
        if (numeroDeTrack === 8) {
            setNumeroDeTrack(1)
            setMusica(playList[0])
        } else {
            setNumeroDeTrack(numeroDeTrack + 1)
            function track() {
                return playList[numeroDeTrack]
            }
            /* const siguienteTrack = playList[numeroDeTrack] */
            setMusica(track)
        }
    }




    return (
        <div>
            <div className='divCointainer'>
                <video autoPlay loop muted preload='auto' id='videoFondo' >
                    <source src={VideoLanding2} type="video/mp4" codecs="avc1.42E01E, mp4a.40.2"  preload='auto'/>
                </video>
            </div>




            <div className='divInfoAzul'>

                <Nav />
                <img src={enzo} className='img' alt={enzo} />
                <h1 className='font-link1' >Bienvenidos a mi Portfolio</h1>

                <div className='reproductor'>

                    <ReactAudioPlayer
                        src={cambiamelamusica()}
                        autoPlay
                        controls
                        loop
                        preload='auto'

                    />
                </div>
                <div className="divazul2">

                    <div className='font-link'>Hola, mi nombre es Enzo Vazquez. Soy Desarrollador Full Stack y este es mi portfolio. Aquí subo a menudo mis últimos proyectos y los enlaces a los mismos. Me encanta el Desarrollo de Software, considero que tengo buenas habilidades técnicas y buenas habilidades blandas. Trabajar en equipo es una de las cosas que más disfruto hacer ya que me potencia a seguir creciendo profesionalmente. 
                    Aprendo sobre la marcha, siempre invento nuevas formas de hacer actividades y presentar ideas. Soy muy responsable, si veo que no cuento con los conocimientos necesarios para la realización de una tarea lo investigo hasta que obtengo los mismos para completarla. Abarco conocimientos, formales y empíricos, para accionar frente al análisis de requerimientos, tanto en el lado del cliente como del servidor.
                    
                    </div>

                    <div className='divProyect'>
                        <div className="cardDiv">

                            <h2 className="TitleProyect">Proyecto Adidas Ecommerce</h2>


                            <Link href="https://deploy-front-pf-ecommerce-adidas.vercel.app/" target="_blank" >
                                <img className='ImagenProyectos' src={pf} alt={pf} />

                            </Link>

                        </div>
                        <div className="cardDiv">

                            <h2 className="TitleProyect">Proyecto Flight Academy</h2>


                            <Link href="https://deploy-front-pf-ecommerce-adidas.vercel.app/" target="_blank" >
                                <img className='ImagenProyectos' src={flight} alt={flight} />

                            </Link>

                        </div>
                        <div className="cardDiv">

                            <h2 className="TitleProyect">Proyecto Pokemon</h2>


                            <Link href="https://deploy-front-pf-ecommerce-adidas.vercel.app/" target="_blank" >
                                <img className='ImagenProyectos' src={pokemon} alt={pokemon} />

                            </Link>

                        </div>
                        <div className="cardDiv">
                            <h2 className="TitleProyect">Aluxioners App</h2>

                            <Link href="https://deploy-front-pf-ecommerce-adidas.vercel.app/" target="_blank" >
                                <img className='ImagenProyectos' src={aluxion} alt={aluxion} />

                            </Link>

                        </div>
                        <div className="cardDiv">

                            <h2 className="TitleProyect">Proyecto Food</h2>


                            <Link href="https://deploy-front-pf-ecommerce-adidas.vercel.app/" target="_blank" >
                                <img className='ImagenProyectos' src={food} alt={food} />
                            </Link>



                        </div>
                    </div>
                </div>
            </div>



            {/*   </div> */}

            {/* <button onClick={handlerPlayList}>Siguiente Pista</button> */}


            {/* <button onClick={console.log("hola")}>Pausar Musica</button> */}
        </div>
    )
}

export default Landing;